import java.util.*;
import java.util.stream.Collectors;

public class Puzzle {

   private String addend1;
   private String addend2;
   private String sum;

   private final Map<Character ,Integer> possibleSolutions = new HashMap<>();
   private final List<Map<Character ,Integer>> foundSolutions = new ArrayList<>();

   /** Solve the word puzzle.
    * @param args three words (addend1 addend2 sum)
    */
   public static void main (String[] args) {
      Puzzle pz = new Puzzle();
      pz.validate(args);

      pz.addend1 = args[0];
      pz.addend2 = args[1];
      pz.sum = args[2];
      pz.solve();
   }

   // https://stackoverflow.com/questions/5238491/check-if-string-contains-only-letters
   private void validate(String[] args) {
       for (String a: args) {
           if (a.length() > 18) {
               throw new IllegalArgumentException("Wrong expression: " + Arrays.toString(args) + " in " + a
                       + " Each word must be less than 18 characters long");
           }
           if (!a.matches("[A-Z]+")) {
               throw new IllegalArgumentException("Wrong expression: " + Arrays.toString(args) + " in " + a
                       + " Must contain only uppercase letters");
           }
       }
   }

   private void solve() {
      findPossibleSolutions(getUniqueLetters());

      System.out.printf("For equation %s + %s = %s, found %d solution(s)",
              addend1, addend2, sum, foundSolutions.size());
      System.out.println();
      for (Map<Character, Integer> sol: foundSolutions) System.out.println(sol);

   }

   private List<Character> getUniqueLetters(){
      Set<Character> uniqueLetters = new HashSet<>();

      for (Character c: addend1.toCharArray()) uniqueLetters.add(c);
      for (Character c: addend2.toCharArray()) uniqueLetters.add(c);
      for (Character c: sum.toCharArray()) uniqueLetters.add(c);

      return new ArrayList<>(uniqueLetters);
   }

   //https://gist.github.com/AhmadElsagheer/d96d1081af545834e32f3624b06c762a
   //https://leetcode.com/problems/generate-parentheses/discuss/10100/Easy-to-understand-Java-backtracking-solution
   //https://coderanch.com/t/595306/java/Optimizing-send-money-code
   private void findPossibleSolutions(List<Character> remainingLetters) {
      if (remainingLetters.isEmpty()) {
         check();
         return;
      }

      for (int i = 0; i < 10; i++) {
         if (possibleSolutions.containsValue(i)) continue;
         possibleSolutions.put(remainingLetters.get(0), i);
         findPossibleSolutions(remainingLetters.stream().skip(1).collect(Collectors.toList()));
         possibleSolutions.remove(remainingLetters.get(0));
      }
   }

   private void check() {
        StringBuilder numberAddend1 = new StringBuilder();
        StringBuilder numberAddend2 = new StringBuilder();
        StringBuilder numberSum = new StringBuilder();

        for (Character c: addend1.toCharArray()) numberAddend1.append(possibleSolutions.get(c).toString());
        for (Character c: addend2.toCharArray()) numberAddend2.append(possibleSolutions.get(c).toString());
        for (Character c: sum.toCharArray()) numberSum.append(possibleSolutions.get(c).toString());

        long a = Long.parseLong(numberAddend1.toString());
        long b = Long.parseLong(numberAddend2.toString());
        long c = Long.parseLong(numberSum.toString());

        if (numberAddend1.charAt(0) != '0' && numberAddend2.charAt(0) != '0' && numberSum.charAt(0) != '0'
               && a + b == c ) {
            foundSolutions.add(new HashMap<>(possibleSolutions));
        }
   }
}

